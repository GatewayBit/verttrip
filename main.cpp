#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

int main() {
    sf::RenderWindow window(sf::VideoMode(800, 600), "Vertex Array");

    sf::Clock clock;

    sf::VertexArray triangle(sf::Quads, 4);

    triangle[0].position = sf::Vector2f(0.f, 0.f);
    triangle[1].position = sf::Vector2f(800.f, 0.f);
    triangle[2].position = sf::Vector2f(800.f, 600.f);
    triangle[3].position = sf::Vector2f(0.f, 600.f);

    triangle[0].color = sf::Color::Red;
    triangle[1].color = sf::Color::Blue;
    triangle[2].color = sf::Color::Green;
    triangle[3].color = sf::Color::Yellow;

    bool enableTrip = false;

    sf::Time lastTick;
    int red = 255;
    int green = 255;
    int blue = 255;
    int redVal = 1;
    int blueVal = 1;
    int greenVal = 1;

    while (window.isOpen()) {
        sf::Time elapsed = clock.restart();
        sf::Event event;
        while (window.pollEvent(event)) {
            switch(event.type) {
                case sf::Event::Closed:
                    window.close();
                    break;
                case sf::Event::KeyPressed:
                    if (event.key.code == sf::Keyboard::Escape) {
                        window.close();
                    }

                    if (event.key.code == sf::Keyboard::Space) {
                        if (enableTrip) {
                            enableTrip = false;
                        } else {
                            enableTrip = true;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        if (enableTrip) {
            lastTick += elapsed;

            if (lastTick.asMilliseconds() > 50) {
                red += redVal;
                blue += blueVal;
                green += greenVal;
                lastTick = sf::Time::Zero;
            }

            // TODO: Randomize each frame.
            if (red > 255) {
                redVal = -3;
                red = 255;
            } else if (red < 0) {
                redVal = 3;
                red = 0;
            }

            if (green > 255) {
                greenVal = -6;
                green = 255;
            } else if (green < 0) {
                greenVal = 6;
                green = 0;
            }

            if (blue > 255) {
                blueVal = -12;
                blue = 255;
            } else if (blue < 0) {
                blueVal = 12;
                blue = 0;
            }

            triangle[0].color = sf::Color(red, 0, 0, 255);
            triangle[1].color = sf::Color(0, green, 0, 255);
            triangle[2].color = sf::Color(0, 0, blue, 255);
            triangle[3].color = sf::Color(red, green, blue, 255);
        }

        window.clear(sf::Color::Black);

        window.draw(triangle);

        window.display();
    }
    return 0;
}
