main : main.o
	g++ -o vertTrip main.o -lsfml-graphics -lsfml-window -lsfml-system

main.o : main.cpp 
	g++ -c main.cpp

clean : 
	rm vertTrip main.o 
